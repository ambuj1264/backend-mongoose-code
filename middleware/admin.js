const isSuperAdmin = (req,res , next)=>{
    if(req.role === "SuperAdmin"){
        next();
    }else{
        return res.status(400).json({msg: "You are not SuperAdmin"});
    }
}
module.exports = isSuperAdmin;