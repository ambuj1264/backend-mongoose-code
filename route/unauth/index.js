const express=require("express")
const router=express.Router();
const itemList=require("../../controllers/crudOpration/index")
const {Registration,Login}=require("../../controllers/userController/index");
const {paymentVerification}=require('../../controllers/integration/index')
router.post("/login", Login.login);
router.post("/register", Registration.registration);
router.post("/paymentverification",paymentVerification);
router.post("/item/manager", itemList.manager);
module.exports=router;
